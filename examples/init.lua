local BASE = (...)..'.'

local endswith = require('lib.gear.strings').endswith
local yui = require 'lib.yui'

local Button = yui.Button
local Rows = yui.Rows
local Ui = yui.Ui

local Examples = {}
Examples.__index = Examples


local function isexample(file)
    return file ~= 'init.lua' and endswith(file, '.lua')
end

local function loadexample(file)
    love.event.clear()
    for i in ipairs(love.handlers) do
        love.handlers[i] = nil
    end

    -- Restart to next example.
    require(BASE..file)
    if love.load then love.load() end
end

local W = 400
local RH = 32

local function makeSelectionMenu()
    local menu = Rows {}

    local files = love.filesystem.getDirectoryItems('examples')
    table.sort(files)

    for _,file in ipairs(files) do
        if isexample(file) then
            local name = file:sub(1, -5)

            menu[#menu+1] = Button {
                w = W, h = RH,

                text = name,
                notranslate = true,
                onHit = function() loadexample(name) end
            }
        end
    end
    menu[#menu+1] = Button {
        w = W, h = RH,

        text = "Quit",
        onHit = function() love.event.quit() end
    }

    local x = math.floor(love.graphics.getWidth() - W) / 2
    local y = math.floor(love.graphics.getHeight() - RH * #menu) / 2

    return Ui:new {
        x = x, y = y,
        menu
    }
end

function love.load()
    gui = makeSelectionMenu()
end

function love.update(dt)
    gui:update(dt)
end

function love.draw()
    gui:draw()
end

return Examples
