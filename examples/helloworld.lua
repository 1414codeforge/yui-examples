-- The ubiquitous "Hello, World!" demo.
-- 'nuff said.
--
-- Layout: Rows
-- Widgets: Label, Button
-- Relevant UI construction code in: love.load()

local yui = require 'lib.yui'

-- Some convenience aliases
local Ui = yui.Ui
local Rows = yui.Rows
local Button, Label = yui.Button, yui.Label

local function centerRectOnScreen(w, h)
    local x = math.floor((love.graphics.getWidth() - w) / 2)
    local y = math.floor((love.graphics.getHeight() - h) / 2)

    return x, y
end

function love.load()
    local W, H = 400, 80  -- pick arbitrary UI size
    local x, y = centerRectOnScreen(W, H)

    gui = Ui:new {
        x = x, y = y,

        Rows {
            Label {
                w = W, h = H,

                text = "Hello, World!"
            },
            Button {
                text = "OBEY",
                onHit = function () love.event.quit() end
            }
        }
    }
end

function love.update(dt)
    gui:update(dt)
end

function love.draw()
    -- Pretty black out there, isn't it?
    -- See more complete examples for shinier stuff :)
    gui:draw()
end
