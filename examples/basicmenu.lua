--- Basic menu example
--
-- Trivial menu made of buttons.
-- Illustrates onHit() and onEnter()/onLeave()
-- events.
--
-- Relevant code to build UI inside: makeMainMenu().
--
-- Layout: Rows
-- Widget: Button

local yui = require 'lib.yui'

local Button = yui.Button
local Rows = yui.Rows
local Ui = yui.Ui

local GUI_WIDTH = 500
local GUI_HEIGHT = 300
local FONT_SIZE = 32

local function centerRectOnScreen(w, h)
    local x = math.floor((love.graphics.getWidth() - w) / 2)
    local y = math.floor((love.graphics.getHeight() - h) / 2)

    return x, y
end

-- Creates main menu.
local function makeMainMenu()
    -- Position the UI
    local x,y = centerRectOnScreen(GUI_WIDTH, GUI_HEIGHT)
    local rh = GUI_HEIGHT / 5  -- 5 elements along height

    -- Keep track what's being focused...
    widgetEnter = function (w)
        guiStatus.current = "You're hovering \""..w.text.."\", feel like pressing it?"
    end
    widgetLeave = function (w)
        guiStatus.previous = "So you left \""..w.text.."\"..."
    end

    return Ui:new {
        x = x, y = y,  -- Place UI at the calculated spot

        -- Place the elements in rows from top to bottom
        Rows {
            Button {
                -- Provide first button's size...
                w = GUI_WIDTH, h = rh,

                text = "Start game",

                onHit = function ()
                    guiStatus = { current = "Game started!" }
                end,
                onEnter = widgetEnter,
                onLeave = widgetLeave
            },
            Button {
                -- ...subsequent widgets _in the same layout_
                -- take last widget's size by default.
                text = "Continue",

                onHit = function()
                    guiStatus = { current = "Loading game..." }
                end,
                onEnter = widgetEnter,
                onLeave = widgetLeave
            },
            Button {
                text = "Options",

                onHit = function()
                    guiStatus = { current = "Options pressed." }
                end,
                onEnter = widgetEnter,
                onLeave = widgetLeave
            },
            Button {
                text = "Credits",

                onHit = function()
                    guiStatus = { current = "Showcasing credits 8)" }
                end,
                onEnter = widgetEnter,
                onLeave = widgetLeave
            },
            Button {
                text = "Quit",

                onHit = function () love.event.quit() end,
                onEnter = widgetEnter,
                onLeave = widgetLeave
            }
        }
    }
end

function love.load()
    guiStatus = { current = "Menu is open." }
    guiFont = love.graphics.newFont('fonts/PixelDroidMenu.ttf', FONT_SIZE)
    gui = makeMainMenu()
end

function love.update(dt)
    -- Let the UI update its status
    gui:update(dt)
end

function love.draw()
    love.graphics.setFont(guiFont)

    -- Print UI status
    local y = 0
    if guiStatus.previous ~= nil then
        love.graphics.print(guiStatus.previous, 0, y)
        y = y + guiFont:getHeight()
    end
    love.graphics.print(guiStatus.current, 0, y)

    -- Draw UI
    gui:draw()
end
