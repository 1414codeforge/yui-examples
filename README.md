Yui Examples
============

A collection of code examples for [yui](https://codeberg.org/1414codeforge/yui).

## Contents

* [Hello World](examples/helloworld.lua) - most basic of the basics.
* [Basic Menu](examples/basicmenu.lua) - a simple menu made of buttons, useful for main menus and pause menus.
* [Options Menu](examples/optionsmenu.lua) - comprehensive example, demonstrating several widgets and interactions.

...and the [initial selection](examples/init.lua) menu is a demo in itself :)

## Running the examples

1. Clone this repository.

2. Download **yui** and its dependencies using [crush](https://codeberg.org/1414codeforge/crush):
   ```sh
   lua crush.lua
   ```

3. Start the demo menu with [LÖVE](https://love2d.org):
   ```sh
   love .
   ```

## Acknowledgement

Some examples use [PixelDroid Menu fonts](https://github.com/pixeldroid/fonts),
see [ACKNOWLEDGEMENT](fonts/README.ACKNOWLEDGEMENT) for details.

## License

Zlib, see [LICENSE](LICENSE) for details.
